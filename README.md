# README #

Compiled on VS 2015

Updater forked from https://bitbucket.org/akastrau/updater-c

To **RUN APPLICATON** you have to download Launcher.zip

# What's new: #

* Speed improvements (I used C# Tasks and Parallell ForEach to speed up core functionality)
* Updater
* Animations
* New source code :)
* WPF app instead of old Windows Forms

# TODO: #

* Server for handling connections (running on Linux -> Docker and Mono)
* Notifications and hiding to tray

# DISCLAIMER #

THIS SOFTWARE HAS NEVER BEEN AUDITED OR REVIEWED. IT HAS NOT BEEN TESTED. THE AUTHOR IS AMATEUR AND YOU SHOULD NOT USE THIS SOFTWARE FOR ANYTHING IMPORTANT. 
YOU SHOULD NOT RELY ON THE SOFTWARE TO WORK AT ALL, OR IN ANY PREDICTABLE WAY, NOR SHOULD YOU ASSUME THAT THE FEATURES CLAIMED ARE THE FEATURES IMPLEMENTED.
THIS SOFTWARE IS FULL OF ERRORS, THE ARCHITECTURE AND DESIGN ARE BROKEN. UNLESS SOME EXPERT CLAIMS OTHERWISE.

# Author #

Adrian Kastrau

Gdansk, 2016

# Licence information #

http://creativecommons.org/licenses/by-nc-sa/4.0/

Feel free to contact me at adrkastr@student.pg.gda.pl

# Other resources #

Icon downloaded from: http://www.iconarchive.com/show/windows-8-metro-icons-by-dakirby309/Apps-Email-Chat-Metro-icon.html

Artist: dAKirby309

Iconset: Windows 8 Metro Icons (436 icons)

License: CC Attribution-Noncommercial 4.0

Commercial usage: Not allowed