﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        AppSettings settings = new AppSettings();
        public MainWindow()
        {
            InitializeComponent();
            MessengerInternalData data = new MessengerInternalData();
            //  ShowFirstTour();
            InitMessenger(data);

        }

       void ShowFirstTour()
        {
            FirstTour tour = new FirstTour();
            Hide();
            tour.ShowDialog();
            settings.FirstLaunch = false;
            JsonSettings.GenerateAppSettingsToFile(settings);
        }

        async void InitMessenger(MessengerInternalData data)
        {
            await Task.Delay(5000);
            Hide();
            MessengerInitView init = new MessengerInitView(ref data);
            init.ShowDialog();
            await Task.Delay(1500);
            data = init.GetInternalMessengerData();

            Directory.CreateDirectory(Environment.ExpandEnvironmentVariables("%AppData%") + "\\SNM4AD\\");

            if (File.Exists((Environment.ExpandEnvironmentVariables("%AppData%") + "\\SNM4AD\\AppSettings.json")))
            {
                string text = File.ReadAllText((Environment.ExpandEnvironmentVariables("%AppData%") + "\\SNM4AD\\AppSettings.json"));
                settings = JsonSettings.GetAppSettings(text);
            }
            else
            {
                settings.FirstLaunch = true;
                JsonSettings.GenerateAppSettingsToFile(settings);
            }

            if (settings.FirstLaunch)
            {
                ShowFirstTour();
            }

            if (data.UserMode == "0")
            {
                data.DataPort = 1221;
                data.ListeningPort = 1222;
                ClientView view = new ClientView(data);
                Hide();
                view.Show();
            }
            else if (data.UserMode == "1")
            {
                data.DataPort = 1222;
                data.ListeningPort = 1221;
                AdminView view = new AdminView(data);
                Hide();
                view.Show();
            }
            else
            {
                MessageBox.Show("Nie można określić trybu pracy!", "Błąd krytyczny",
                                       MessageBoxButton.OK, MessageBoxImage.Stop);
                Application.Current.Shutdown(1);
                return;
            }
            
        }

    }
}
