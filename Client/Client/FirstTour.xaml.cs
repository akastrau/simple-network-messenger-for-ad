﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for FirstTour.xaml
    /// </summary>
    public partial class FirstTour : Window
    {
        public FirstTour()
        {
            InitializeComponent();
            ShowStory();
        }

        async void ShowStory()
        {
            await Task.Delay(6000);
            Storyboard story = (Storyboard)FindResource("onTour2");
            story.Begin(this);

            await Task.Delay(3000);
            story = (Storyboard)FindResource("onTour3");
            story.Begin(this);

            await Task.Delay(3000);
            story = (Storyboard)FindResource("onTour4");
            story.Begin(this);

            await Task.Delay(3000);
            story = (Storyboard)FindResource("onTour5");
            story.Begin(this);

            await Task.Delay(4700);
            story = (Storyboard)FindResource("onTour6");
            story.Begin(this);

            await Task.Delay(4000);
            story = (Storyboard)FindResource("onTour7");
            story.Begin(this);

            await Task.Delay(3000);
            Close();

        }
    }
}
