﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class MessengerInternalData
    {
        public IPAddress IP { get; set; }
        public IPAddress SendIP { get; set; }
        public string HostName { get; set; }
        public string DomainName { get; set; }
        public short ListeningPort { get; set; }
        public short DataPort { get; set; }
        public string UserMode { get; set; }
        public bool isValid { get; set; }
    }
}
