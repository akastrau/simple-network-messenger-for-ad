﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    interface IConnections
    {
        Action ReadDataStream(MessengerInternalData data);
        Action StartListening(MessengerInternalData data, object View);
        Action SendUDPMessage(MessengerInternalData data, string Text, bool SignMessage, object View);
    }
}
