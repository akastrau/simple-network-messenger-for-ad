﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class MessengerInternalDataUtils
    {
        public static bool isValidData(MessengerInternalData Data)
        {
            bool returnData = true;

            Parallel.ForEach(Data.GetType()
                .GetProperties(),
                value =>
                {
                    if (value.GetValue(Data) == null)
                    {
                        returnData = false;
                        return;
                    }
                });
            return returnData;
        }
    }
}
