﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for AdminView.xaml
    /// </summary>
    /// 
    public partial class AdminView : Window
    {
        MessengerInternalData data = new MessengerInternalData();
        public AdminView()
        {
            InitializeComponent();
        }

        public AdminView(MessengerInternalData data)
        {
            InitializeComponent();
            this.data = data;
            richTextBox.IsReadOnly = true;
            richTextBox.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            richTextBox.AppendText(Environment.NewLine + Environment.NewLine);
        }

        private void sendButton_Click(object sender, RoutedEventArgs e)
        {
            if (input.Text.Trim() == "")
            {
                return;
            }
            AdminClass admin = new AdminClass();
            Task SendTask = new Task(admin.SendUDPMessage(data, input.Text, true, this));
            SendTask.Start();
            richTextBox.AppendText("Wysłałeś wiadomość: " + input.Text + Environment.NewLine);
            input.Text = "";
            e.Handled = true;
            return;
        }

        public void UpdateMessageLog(string Text)
        {
            if (Dispatcher.CheckAccess())
            {
                richTextBox.AppendText(Text + Environment.NewLine);
            }
            else
            {
                Dispatcher.Invoke(() =>
                {
                    richTextBox.AppendText(Text + Environment.NewLine);
                });
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            AdminClass admin = new AdminClass();
            Task listen = new Task(admin.StartListening(data, this));
            listen.Start();
            e.Handled = true;
        }

        public void UpdateStatus(string Text)
        {
            if (Dispatcher.CheckAccess())
            {
                StatusLabel.Text = Text;
            }
            else
            {
                Dispatcher.Invoke(() =>
                {
                    StatusLabel.Text = Text;
                });
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown(0);
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Adrian Kastrau, Simple Network Messenger for Active Directory", "O programie", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
