﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Client
{
    static class IPAddressExtensions
    {
        public static IPAddress GetBroadcastAddress(this IPAddress address, IPAddress subnetMask)
        {
            byte[] ipAdressBytes = address.GetAddressBytes();
            byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

            if (ipAdressBytes.Length != subnetMaskBytes.Length)
                throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

            byte[] broadcastAddress = new byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++)
            {
                broadcastAddress[i] = (byte)(ipAdressBytes[i] | (subnetMaskBytes[i] ^ 255));
            }
            return new IPAddress(broadcastAddress);
        }
    }
    static class Utils
    {
        public static string GetDomainName()
        {
            try
            {
                return IPGlobalProperties.GetIPGlobalProperties().DomainName;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd krytyczny", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        public static string GetUserMode()
        {
            try
            {
                PrincipalContext context = new PrincipalContext(ContextType.Domain);
                UserPrincipal user = UserPrincipal.FindByIdentity(context, Environment.UserName);
                string result = null;

                Parallel.ForEach(user.GetGroups(), group =>
                {
                    if (group.Name == "Schema Admins" || group.Name == "Enterprise Admins" || group.Name == "Domain Admins")
                    {
                        result = "1";
                        return;
                    }

                    else if (group.Name == "Domain Users")
                    {
                        result = "0";
                    }
                });
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd krytyczny", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }
        public static IPAddress GetBroadcastAddress(IPAddress localIP)
        {
            IPAddress broadcast = localIP;
            IPAddress subnetMask = null;

            try
            {
                Parallel.ForEach(NetworkInterface.GetAllNetworkInterfaces(), (adapter) =>
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    Parallel.ForEach(properties.UnicastAddresses, (unicast) =>
                    {
                        if (unicast.Address.ToString() == localIP.ToString())
                        {
                            subnetMask = unicast.Address;
                            return;
                        }
                    });
                });
                return broadcast.GetBroadcastAddress(subnetMask);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd krytyczny", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }
        public static string GetHostName()
        {
            try
            {
                return Dns.GetHostName();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd krytyczny", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }
        public static IPAddress GetLocalIPAddress()
        {
            try
            {
                if (!NetworkInterface.GetIsNetworkAvailable())
                {
                    return null;
                }
                IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
                return host.AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd krytyczny", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }
    }
}
