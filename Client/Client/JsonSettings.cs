﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Windows;

namespace Client
{
    class JsonSettings
    {
        public static AppSettings GetAppSettings(string data)
        {
            try
            {
                return JsonConvert.DeserializeObject<AppSettings>(data);
            }
            catch (Exception)
            {
                AppSettings info = new AppSettings();
                return info;
            }
        }

        public static void GenerateAppSettingsToFile(AppSettings app)
        {
            try
            {
                JsonSerializer serializer = new JsonSerializer();
                using (StreamWriter DataFile = new StreamWriter((Environment.ExpandEnvironmentVariables("%AppData%")) + "\\SNM4AD\\AppSettings.json", false))
                using (JsonWriter writer = new JsonTextWriter(DataFile))
                {
                    writer.Formatting = Formatting.Indented;
                    serializer.Serialize(writer, app);
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
               MessageBox.Show(ex.Message, "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
