﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for MessengerInitView.xaml
    /// </summary>
    public partial class MessengerInitView : Window
    {
        private MessengerInternalData ThisInternalData = new MessengerInternalData();

        public MessengerInitView()
        {
            InitializeComponent();
        }

        public MessengerInitView(ref MessengerInternalData data)
        {
            InitializeComponent();
            Task task = new Task(async () =>
           {
               await Task.Delay(1500);
               ChangeStatus("Trwa logowanie");
               ThisInternalData.DataPort = 1221;
               ThisInternalData.DomainName = Utils.GetDomainName();
               ThisInternalData.HostName = Utils.GetHostName();
               ThisInternalData.IP = Utils.GetLocalIPAddress();
               ThisInternalData.ListeningPort = 1222;
               ThisInternalData.SendIP = Utils.GetBroadcastAddress(ThisInternalData.IP);
               ThisInternalData.UserMode = Utils.GetUserMode();//TODO: Test
               ThisInternalData.isValid = MessengerInternalDataUtils.isValidData(ThisInternalData);
               if (!ThisInternalData.isValid)
               {
                   MessageBox.Show("Nie udało się zainicjować wszystkich potrzebnych komponentów!", "Błąd krytyczny",
                       MessageBoxButton.OK, MessageBoxImage.Stop);
                   Dispatcher.Invoke( () =>
                   {
                       Application.Current.Shutdown(1);
                   });
                   return;
               }
               StopProgress();
               ChangeStatus("Zaraz zaczynamy...");
               await Task.Delay(3000);
               Task SubmitData = new Task(() =>
               {
                   Dispatcher.Invoke(async () =>
                   {
                       OnCloseAnim();
                       await Task.Delay(1000);
                       Close();
                   });
               });
               SubmitData.Start();
           });
            task.Start();
           
        }

        void ChangeStatus(string Text)
        {
            if (Dispatcher.CheckAccess())
            {
                statusLabel.Text = Text;
                statusLabel.Visibility = Visibility.Visible;
                Storyboard story = (Storyboard)FindResource("ChangeStatus");
                story.Begin(this);
            }
            else
            {
                Dispatcher.Invoke(() =>
                {
                    statusLabel.Text = Text;
                    statusLabel.Visibility = Visibility.Visible;
                    Storyboard story = (Storyboard)FindResource("ChangeStatus");
                    story.Begin(this);
                });
            }
        }

        void StopProgress()
        {
            if (Dispatcher.CheckAccess())
            {
                Storyboard story = (Storyboard)FindResource("animDots");
                story.Stop(this);
            }
            else
            {
                Dispatcher.Invoke(() =>
                {
                    Storyboard story = (Storyboard)FindResource("animDots");
                    story.Stop(this);
                });
            }
        }

        void OnCloseAnim()
        {
            if (Dispatcher.CheckAccess())
            {
                Storyboard story = (Storyboard)FindResource("OnClose");
                story.Begin(this);
            }
            else
            {
                Dispatcher.Invoke(() =>
                {
                    Storyboard story = (Storyboard)FindResource("OnClose");
                    story.Begin(this);
                });
            }
        }

        public MessengerInternalData GetInternalMessengerData()
        {
            return ThisInternalData;
        }
    }
}
