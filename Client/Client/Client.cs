﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows;

namespace Client
{
    class ClientClass : IConnections
    {
       public Action ReadDataStream(MessengerInternalData data)
        {
            return new Action(() => { }); //todo:Implement
        }

       public Action SendUDPMessage(MessengerInternalData data, string Text, bool SignMessage, object client)
        {
            return new Action(() =>
            {
                Text.Replace(Environment.NewLine, "");
                Socket udp = null;
                IPEndPoint ipEndPoint = null;
                ClientView view = (ClientView)client;

                try
                {
                    udp = new Socket(SocketType.Dgram, ProtocolType.Udp); //TODO: check RDM
                    ipEndPoint = new IPEndPoint(data.SendIP, data.DataPort);

                    if (SignMessage)
                    {
                        string msg = "Wiadomość od " + Environment.UserName + "@" + data.DomainName + ": " + Text;
                        Text = msg;
                        byte[] MessageBytes = Encoding.UTF8.GetBytes(Text);
                        udp.SendTo(MessageBytes, ipEndPoint);
                        udp.Dispose();
                        view.UpdateStatus("Wysłano");
                        return;
                    }
                    else
                    {
                        byte[] MessageBytes = Encoding.UTF8.GetBytes(Text);
                        udp.Send(MessageBytes);
                        udp.Dispose();
                        view.UpdateStatus("Wysłano");
                        return;
                    }
                }
                catch (Exception ex)
                {
                    view.UpdateStatus("Błąd wysyłania wiadomości");
                    MessageBox.Show(ex.Message, "Błąd krytyczny", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            });
        }

        public Action StartListening(MessengerInternalData data, object View)
        {
            return new Action(() =>
            {
                IPEndPoint ipEndPoint = null;
                UdpClient udp = null;
                ClientView client = (ClientView)View;

                try
                {
                    ipEndPoint = new IPEndPoint(IPAddress.Any, 0);
                    udp = new UdpClient(data.ListeningPort);
                    client.UpdateStatus("Gotowy!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Błąd krytyczny", MessageBoxButton.OK, MessageBoxImage.Error);
                    client.UpdateStatus("Błąd! Nie można odbierać wiadomości!");
                    return;
                }

                while (true)
                {
                    try
                    {
                        byte[] message = udp.Receive(ref ipEndPoint);
                        string encodedMessage = Encoding.UTF8.GetString(message);

                        if (encodedMessage.Trim() != "")
                        {
                            client.UpdateStatus("Odebrano wiadomość");
                            client.UpdateMessageLog(encodedMessage);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Błąd krytyczny", MessageBoxButton.OK, MessageBoxImage.Error);
                        client.UpdateStatus("Błąd odbierania wiadomości!");
                        return;
                    }
                }
            });
        }
    }
}
