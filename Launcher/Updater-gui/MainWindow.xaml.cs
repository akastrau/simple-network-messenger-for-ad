﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Updater_gui
{ 
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

            Task laucher = new Task(() => { MainLauncher.launcher(this); });
            laucher.Start();
        }

        public void ChangeStatusText(string Text)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                statusLabel.Content = Text;
                Storyboard story = (Storyboard)FindResource("OnLoaded1");
                story.Begin(this);
            }));
        }

        public void ExitApp()
        {
            Dispatcher.Invoke(()
                =>
            {
                Application.Current.Shutdown(0);
            });
        }

        public void StopProgressDots()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                Storyboard story = (Storyboard)FindResource("animDots");
                story.Remove(this);
                ellipse.Visibility = Visibility.Collapsed;
                ellipse1.Visibility = Visibility.Collapsed;
                ellipse2.Visibility = Visibility.Collapsed;
            }));
        }

        public void ShowErrorMessage(string Text)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                StopProgressDots();
                errorText.Text = Text;
                Storyboard story = (Storyboard)FindResource("errorMessage");
                story.Begin(this);
            }));
        }

        public void ShowExitButton()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                exitButton.Visibility = Visibility.Visible;
            }));
        }

        private void statusLabel_Loaded(object sender, RoutedEventArgs e)
        {
           
        }

        private void statusLabel_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            Storyboard story = (Storyboard)FindResource("OnLoaded1");
            story.Begin(this);
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
