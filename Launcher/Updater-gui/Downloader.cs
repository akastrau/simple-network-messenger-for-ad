﻿using ICSharpCode.SharpZipLib;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Updater_gui
{
    public static class ZipArchiveExtensions
    {
        public static void ExtractToDirectory(this ZipArchive archive, string destinationDirectoryName, bool overwrite)
        {
            if (!overwrite)
            {
                archive.ExtractToDirectory(destinationDirectoryName);
                return;
            }
            foreach (ZipArchiveEntry file in archive.Entries)
            {
                string completeFileName = Path.Combine(destinationDirectoryName, file.FullName);
                if (file.Name == "")
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(completeFileName));
                    continue;
                }
                file.ExtractToFile(completeFileName, true);
            }
        }
    }

    class Downloader
    {
        public static void GetAppFromURL(AppInfo app)
        {
            MainWindow window = MainLauncher.GetMainWindow();
            try
            {
                Directory.CreateDirectory("Core\\bin\\");
                WebClient client = new WebClient();
                window.ChangeStatusText("Pobieranie aplikacji...");

                if (app.RedirectTo != null)
                {
                    client.DownloadFile(app.RedirectTo, app.File);
                    if (!VerifyDownload(app.File, app.Hash)) throw new Exception ("Uszkodzony plik...");
                }
                else
                {
                    client.DownloadFile(app.DownloadURL, app.File);
                    if(!VerifyDownload(app.File, app.Hash)) throw new Exception("Uszkodzony plik...");
                }
                if (app.RequireAdditionalComponents == "yes")
                {
                    window.ChangeStatusText("Pobieranie komponentów...");
                    client.DownloadFile(app.AdditionalComponentsUrl, app.AdditionalComponents);
                    window.ChangeStatusText("Rozpakowywanie...");
                    //Console.WriteLine("\n\tExtracting..." + "\n");
                    FastZip zip = new FastZip();
                    zip.ExtractZip(app.AdditionalComponents, "Core\\bin\\", null);
                    //ZipArchive zip = ZipFile.OpenRead(app.AdditionalComponents);
                    //ZipArchiveExtensions.ExtractToDirectory(zip, Environment.CurrentDirectory, true);
                    //zip.Dispose();
                    File.Delete(app.AdditionalComponents);
                }
            }
            catch (Exception ex)
            {
                if (File.Exists(app.File))
                {
                    File.Delete(app.File);
                }
                window.ShowErrorMessage(ex.Message);
            }
        }

        public static bool VerifyDownload(string FileName, string Checksum)
        {
            if (Calculate_Checksum.GetHashFromFile(FileName, Calculate_Checksum.SHA512) != Checksum) return false;
            return true;
        }
    }
}
