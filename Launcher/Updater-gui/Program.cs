﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Windows;

namespace Updater_gui
{
    public class MainLauncher
    {
        private static string checksum;
        private static MainWindow refWindow;

        public static MainWindow GetMainWindow() { return refWindow; }

        public async static void launcher(MainWindow window)
        {

            // Console.Title = "Updater";
            //You must fill upp fields of class AppInfo before run (If u want generate AppInfo)

            //AppInfo appInfo = new AppInfo();
            //appInfo.AppName = "SNM4AD";
            //appInfo.Author = "Adrian Kastrau";
            //appInfo.Version = "1.0";
            //appInfo.File = "Client.exe";
            //appInfo.DownloadURL = "https://bitbucket.org/akastrau/simple-network-messenger-for-ad/downloads/Client.exe";
            //appInfo.ChangelogText = "Stable release";
            //appInfo.Comments = "Published";
            //appInfo.RequireAdditionalComponents = "yes";
            //appInfo.AdditionalComponents = "libs.zip";
            //appInfo.AdditionalComponentsUrl = "https://bitbucket.org/akastrau/simple-network-messenger-for-ad/downloads/Components.zip";

            //// Console.WriteLine("\n\tCalculating checksum...\n");
            //try
            //{
            //    string checksum = Calculate_Checksum.GetHashFromFile(appInfo.File, Calculate_Checksum.SHA512);
            //    appInfo.Hash = checksum;
            //}
            //catch (Exception)
            //{

            //    //  Console.WriteLine(ex.Message);
            //}

            //Json.GenerateAppInfoToFile(appInfo);


            refWindow = window;
            Task wait = Task.Delay(3500);
            await wait;
            window.ChangeStatusText("Pobieranie informacji z repozytorium...");

            string AppInfo = "";
            DownloadAppInfo.GetFileFromURL("https://bitbucket.org/akastrau/simple-network-messenger-for-ad/downloads/AppInfo.json", ref AppInfo);
            AppInfo info = Json.GetAppInfo(AppInfo);
            info.File = "Core\\bin\\" + info.File;

            try
            {
                checksum = Calculate_Checksum.GetHashFromFile(info.File, Calculate_Checksum.SHA512);
            }
            catch (Exception)
            {
                Downloader.GetAppFromURL(info);
                window.ChangeStatusText("Zakończono! Uruchom aplikacje jeszcze raz!");
                window.StopProgressDots();
                window.ShowExitButton();
                return;
            }

            if (checksum != info.Hash)
            {
                Downloader.GetAppFromURL(info);
                if (info.ChangelogText != null)
                {
                    ///    Console.WriteLine("\n\tChangelog:\n\t{0}\n", info.ChangelogText);
                }
                window.ChangeStatusText("Zakończono!");
                window.StopProgressDots();
                await Task.Delay(3000);
                Process.Start("Core\\bin\\Client.exe");
                window.ExitApp();
            }
            else
            {
                window.ChangeStatusText("NIe znaleziono nowych aktualizacji");
                window.StopProgressDots();
                await Task.Delay(3000);
                Process.Start("Core\\bin\\Client.exe");
                window.ExitApp();
            }
        }
    }
}
